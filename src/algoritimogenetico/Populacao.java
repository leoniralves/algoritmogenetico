/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritimogenetico;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author leoniralves
 */
public class Populacao {
    ArrayList<DNA> populacao;
    int iniX, iniY; // Coordenada do ponto inicial (x == linhas, y == colunas)
    int fimX, fimY; // Coordenada do ponto final (x == linhas, y == colunas)
    int linhas, colunas; // Tamanho da matriz
    int qtd; // Quantidade de DNAs
    int tamanho; // Tamanho do DNA
    Ambiente ambiente;
    
    public Populacao(int linhas, int colunas, int iniX, int iniY, int fimX, int fimY, int qtd, int tamanho) {
        this.linhas = linhas;
        this.colunas = colunas;
        this.iniX = iniX;
        this.iniY = iniY;
        this.fimX = fimX;
        this.fimY = fimY;
        this.qtd = qtd;
        this.tamanho = tamanho;
        
        this.ambiente = new Ambiente(linhas, colunas, iniX, iniY, fimX, fimY);
        
        this.populacao = new ArrayList<>();
        
        for (int i = 0; i < qtd; i++) {
            DNA dna = new DNA(tamanho);
            this.populacao.add(dna);
        }
    }
    
    public void aplicarDNAs() {
        for (DNA dna : populacao) {
            this.ambiente.limparMatriz();
            //this.ambiente.ManualObstaculo();
            this.ambiente.AplicarDNA(dna);
            // dna.show();
            // System.out.print("\n");
        }
    }
    
    public void ordenarDNAs() {
        Collections.sort(populacao, new Comparator<DNA>() {

            @Override
            public int compare(DNA dna1, DNA dna2) {
                return dna1.getTentativas().compareTo(dna2.getTentativas());
            }
        });
    }
    
    public void removerPiores() {
        int init = populacao.size()-1;
        int size = populacao.size()/2;
        for (int i = init; i >= size; i--) {
            populacao.remove(i);
        }
    }
    
    public void show() {
        for (DNA dna : populacao) {
            System.out.println("Erros: "+dna.getErros());
            System.out.println("Tentativas: "+dna.getTentativas());
            System.out.println("Sucesso: "+dna.getSucesso());
            System.out.println("-----------");
        }
    }
    
    public void crossover() {
        int size = populacao.size();
        for (int i = 0; i < size; i++) {
            if (i == (populacao.size() - 1)) {
                populacao.add(populacao.get(i).crossover(populacao.get(0)));
                
            }
            else {
                populacao.add(populacao.get(i).crossover(populacao.get(i+1)));
            }
        }
    }
    
    public void mutacionar() {
        float pop = (float)(this.populacao.size()*0.2);
        int partePopulacao = Math.round(pop);
        for(int i = 0; i < partePopulacao; i++) {
            populacao.get(i).mutacionar();
        }
    }
    
    public int qualidade() {
        int qtdSucesso = 0;
        for(int i = 0; i < populacao.size(); i++) {
            if (populacao.get(i).getSucesso()) {
                qtdSucesso++;
            }
        }
        return (qtdSucesso*100)/populacao.size();
    }
    
    public void algoritimoGenetico() {
        
        do {            
            aplicarDNAs();
            ordenarDNAs();
            System.out.println(qualidade()+"%");
            if (qualidade() >= 80) {
                break;
            }
            else {
                removerPiores();
                crossover();
                mutacionar();
            } 
        } while (true);
        
        DNA best = populacao.get(0);
        ambiente.AplicarDNA(best);
        ambiente.exibir();
        
        
        
    }
}
