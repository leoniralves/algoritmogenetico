/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritimogenetico;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author leoniralves
 */
public class DNA {
    private ArrayList<Integer> dna;
    private int erros;
    private int tentativas;
    private Boolean sucesso;

    public DNA() {
    }

    public DNA(int tamanho) {
        this.dna = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < tamanho; i++) {
            this.dna.add(random.nextInt(8));
        }
    }
    
    public void reset() {
        this.erros = 0;
        this.tentativas = 0;
        this.sucesso = false;
    }
    
    public DNA crossover(DNA x) {
        DNA dnaAux = new DNA();
        dnaAux.setDna(new ArrayList<>());
        Random random = new Random();
        for (int i = 0; i < this.getDna().size(); i++) {
            if (random.nextInt(2) % 2 == 0) {
                dnaAux.getDna().add(this.getDna().get(i));
            }
            else {
                dnaAux.getDna().add(x.getDna().get(i));
            }
        }
        return dnaAux;
    }
    
    public void mutacionar() {
        Random random = new Random();
        this.dna.set(random.nextInt(this.dna.size()-1), random.nextInt(7));
    }
    
    public void show() {
        for (Integer value : getDna()) {
            System.out.print("["+value+"] ");
        }
    }

    /**
     * @return the erros
     */
    public Integer getErros() {
        return erros;
    }

    /**
     * @return the tentativas
     */
    public Integer getTentativas() {
        return tentativas;
    }

    /**
     * @return the sucesso
     */
    public Boolean getSucesso() {
        return sucesso;
    }

    /**
     * @param erros the erros to set
     */
    public void setErros(int erros) {
        this.erros += erros;
    }

    /**
     * @param tentativas the tentativas to set
     */
    public void setTentativas(int tentativas) {
        this.tentativas += tentativas;
    }

    /**
     * @param sucesso the sucesso to set
     */
    public void setSucesso(Boolean sucesso) {
        this.sucesso = sucesso;
    }

    /**
     * @return the dna
     */
    public ArrayList<Integer> getDna() {
        return dna;
    }

    /**
     * @param dna the dna to set
     */
    public void setDna(ArrayList<Integer> dna) {
        this.dna = dna;
    }
}
