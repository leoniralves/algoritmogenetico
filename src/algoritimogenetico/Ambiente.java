/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritimogenetico;

/**
 *
 * @author leoniralves
 */
public class Ambiente {
    public char m[][];
    int iniX, iniY; // Coordenada do ponto inicial (x == linhas, y == colunas)
    int fimX, fimY; // Coordenada do ponto final (x == linhas, y == colunas)
    int linhas, colunas; // Tamanho da matriz
    private DNA dnaCopy = new DNA();
    private int x = 0;
    private int y = 0;

    // Construtor: instancia matriz e propriedades
    public Ambiente(int linhas, int colunas, int iniX, int iniY, int fimX, int fimY) {
        this.linhas = linhas;
        this.colunas = colunas;
        this.iniX = iniX;
        this.iniY = iniY;
        this.fimX = fimX;
        this.fimY = fimY;
        
        this.m = new char[linhas][colunas];
    }
    
    // Preenche a matriz com char ESPAÇO
    public void limparMatriz() {
        for(int i = 0; i < linhas; i++) {
            for (int j = 0; j < colunas; j++) {
                m[i][j] = ' ';
            }
        }
    }
    
    public boolean percorrer(int x, int y) {
        if (!this.validaPonto(x, y) || this.getPonto(x, y) == '#' || this.getPonto(x, y) == '-') {
            dnaCopy.setErros(1);
        }
        else {
            if (this.fimX == x && this.fimY == y) {
                dnaCopy.setSucesso(true);
                return true;
            }
            else {
                this.m[x][y] = '-';
            }
            this.x = x;
            this.y = y;
        }
        return false;
    }
    
    public void AplicarDNA(DNA dna) {
        dna.reset();
        limparMatriz();
        ManualObstaculo();
        dnaCopy = dna;
        boolean criterioDeParada = false;
        
        x = y = 0;
        
        for (int i = 0; i < dna.getDna().size(); i++) {
            dnaCopy.setTentativas(1);
            switch(dna.getDna().get(i)) {
                case 0:
                    // x-1, y-1
                    criterioDeParada = this.percorrer(x-1, y-1);
                    break;
                case 1:
                    // x-1, y
                    criterioDeParada = this.percorrer(x-1, y);
                    break;
                case 2:
                    // x-1, y+1
                    criterioDeParada = this.percorrer(x-1, y+1);
                    break;
                case 3:
                    // x, y-1
                    criterioDeParada = this.percorrer(x, y-1);
                    break;
                case 4: 
                    // x, y+1
                    criterioDeParada = this.percorrer(x, y+1);
                    break;
                case 5:
                    // x+1, y-1
                    criterioDeParada = this.percorrer(x+1, y-1);
                    break;
                case 6:
                    // x+1, y
                    criterioDeParada = this.percorrer(x+1, y);
                    break;
                case 7:
                    // x+1, y+1
                    criterioDeParada = this.percorrer(x+1, y+1);
                    break;
            }
//            System.out.println("Caminho: "+dna.getDna().get(i));
//            this.exibir();
            if (criterioDeParada) {
                break;
            }
        }
    }
    
    public void ManualObstaculo() {
        m[7][2] = m[7][1] = m[6][1] = '#';
        m[3][7] = m[3][8] = m[3][9] = '#';
        m[4][3] = m[5][3] = m[5][4] = '#';
        m[2][6] = m[3][6] = m[5][5] = '#';
        m[7][3] = m[5][6] = m[7][4] = '#';
    }
    
    // varifica se o ponto x,y esta na matriz e se esta vazio (ESPAÇO)
    public boolean validaPonto(int x, int y) {
        if (x >= 0 && y >= 0 && x < linhas && y < colunas) {
            if (m[x][y] == ' ') {
                return true;
            }
        }
        return false;
    }
    
    // Exibe a matriz na tela
    public void exibir() {
        for (int i = 0; i < linhas; i++) {
            for (int j = 0; j < colunas; j++) {
                System.out.print("["+ m[i][j] +"]");
            }
            System.out.print("\n");
        }
        System.out.print("\n\n");
    }
    
    // seta o valor no ponto case este esteja dentro da matriz e seja vazio (ESPAÇO)
    public boolean setPonto(int x, int y, char valor) {
        if (validaPonto(x, y)) {
            m[x][y] = valor;
            return true;
        }
        return false;
    }
    
    // retorna valor char relativo ao ponto x,y
    public char getPonto(int x, int y) {
        return m[x][y];
    }
}